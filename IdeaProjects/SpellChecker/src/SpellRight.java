import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class SpellRight {
    public static void main(String[]args)throws Exception{
        Scanner scanner = new Scanner(System.in);
        String fileName = scanner.next();

        String[] linesArr = fileToLines(fileName);

        String[] dictionaryArr = dictionary();
        String lineToParse="";
        int i = 0;
        Scanner arrayScanner = new Scanner(linesArr[i]);
        for (i = 0; i <= linesArr.length; i++) {
            while (arrayScanner.hasNext()) {
                String word = arrayScanner.next();
                word = word.toLowerCase();
                System.out.println(word);
                if(!Arrays.asList(dictionaryArr).contains(word))
                    System.out.println("line    "+(i+1)+": "+word);

            }
        }
    }
    private static String[] fileToLines(String fileName)throws Exception {
        Scanner file = new Scanner(new File(fileName));

        List<String> lines = new ArrayList<>();
        while (file.hasNextLine()) {
            lines.add(file.nextLine());
        }
        String[] linesArr = new String[lines.size()];
        linesArr = lines.toArray(linesArr);
        return linesArr;
    }

    private static String[] dictionary()throws Exception{
        File dictFile = new File ("Dictionary.text");
        Scanner dictReader = new Scanner (dictFile);

        List<String> dictionaryList = new ArrayList<>();
        while(dictReader.hasNext()){
            String dictionaryElement = dictReader.next();
            dictionaryElement = dictionaryElement.toLowerCase();
            dictionaryList.add(dictionaryElement);
        }
        String[] dictionaryArr = new String[dictionaryList.size()];
        dictionaryArr = dictionaryList.toArray(dictionaryArr);
        return dictionaryArr;
    }
}

